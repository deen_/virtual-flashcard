![Virtual Flashcard](/public/img/icon.png "VF logo")

# Virtual Flashcard

VF is an application to recreate a conventional flashcard into a virtual learning tool. 

The app includes an admin page where user can customize and make their own presentations and decks that suits their own learning style and needs. 

The application consumes VF API 
https://bitbucket.org/deen_/virtual-flashcard-api/src/master/

## Install

```
git clone https://deen_@bitbucket.org/deen_/virtual-flashcard-api.git
```


## Run the App

Run this at the root of the directory
```
npm run start
```

## Usage

Visit http://localhost:3000/