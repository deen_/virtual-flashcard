const path = require('path')
const express = require('express')
const hbs = require('hbs')
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');

const app = express()
const port = process.env.PORT || 3000

// Define paths for Express config  
const publicDirectoryPath = path.join(__dirname, '../public')
const viewsPath = path.join(__dirname, '../templates/views')
const partialsPath = path.join(__dirname, '../templates/partials')

// Setup handlebars engine and view location
app.set('view engine', 'hbs')
app.set('views', viewsPath)
hbs.registerPartials(partialsPath)

// Setup static directory to serve
app.use(express.static(publicDirectoryPath))

app.use(bodyParser.json());
app.use(cookieParser());

app.get('', (req, res) => {
    res.render('index', {
        mainPage: true,
    })
})

app.get('/admin', (req, res) => {
    if (req.cookies.userToken) {
        res.render('admin', {
            mainPage: false,
        })    
    } else {
        res.render('index', {
            mainPage: true,
        })  
    }    
})

app.listen(port, () => {
    console.log('Server is up on port ' + port)
})