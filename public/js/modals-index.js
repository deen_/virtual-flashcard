const modalSettings = document.getElementsByClassName("modal-settings")
const loginModal = document.querySelector(".modal-login-register")
const changeDeckModal = document.querySelector(".modal-change-deck")
const adminLink = document.querySelector(".admin-link")
const loginRegisterLink = document.querySelector(".login-register-link")


for (var i = 0; i < modalSettings.length; i++) {
    modalSettings[i].addEventListener('click', (e) => {
    
        changeDeckModal.style.display = "block";
        if ( logged_in() ) { 
            adminLink.style.display = "block";
            loginRegisterLink.style.display = "none";
        } else {
            adminLink.style.display = "none";
            loginRegisterLink.style.display = "flex";
        }
    });
}
