var perPage = 9;
var totalPages = 1;
var currentPage = 1;
var startPage = (currentPage * perPage) - perPage;
var endPage = startPage + perPage; 

const display_pagination = () => {
    pagination.innerHTML = "";
    outOfRange = false;

    pagination.innerHTML += `<li class="pagination-text prev-page${(currentPage == 1 ? " disabled" : "")}"><a href="javascript:void(0)"><<</a></li>`;
    
    for ( i=1; i<=totalPages; i++ ) {
        if (i <= 2 || i >= totalPages - 2 || Math.abs(i - currentPage) <= 2) {
            $outOfRange = false;
            pagination.innerHTML += `<li class="pagination-num${(currentPage == i ? " active" : "")}"><a href="javascript:void(0)">${i}</a></li>`;
        } else {
            if ( !outOfRange ) pagination.innerHTML += "..."; 
            outOfRange = true;
        }
    }
    pagination.innerHTML += `<li class="pagination-text next-page${(currentPage == totalPages ? " disabled" : "")}"><a href="javascript:void(0)">>></a></li>`;
    
}

const update_active_page = () => {
    const pages = document.getElementsByClassName('pagination-num');
    const activePage = document.querySelector('.pagination-num.active');
    if (activePage) activePage.classList.remove('active');

    Array.from(pages).filter( page => {
        if (page.textContent == currentPage ) {
            page.classList.add('active');
        }
    })
}

const refresh_page = () => {
    startPage = (currentPage * perPage) - perPage;
    endPage = startPage + perPage; 
    update_active_page();

    // display_cards();
    for (i=0; i<cardItems.length; i++) {
        if (i>=startPage && i<endPage) cardItems[i].style.display = "block";
        else cardItems[i].style.display = "none";
    }
}

document.addEventListener('click',function(e) {
    if ( e.target && (e.target.classList.contains('pagination-num') || e.target.parentNode.classList.contains('pagination-num')) ){
        currentPage = Number(e.target.textContent);
        refresh_page();
    }  
    else if ( e.target && (e.target.classList.contains('prev-page') || e.target.parentNode.classList.contains('prev-page')) ){
        if ( currentPage != 1 ) {
            currentPage--;
            refresh_page();
        }
    }
    else if ( e.target && (e.target.classList.contains('next-page') || e.target.parentNode.classList.contains('next-page')) ){
        
        if ( currentPage != totalPages ) {
            currentPage++;
            refresh_page();
        }
    }
})