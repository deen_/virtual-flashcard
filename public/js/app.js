// const url = 'http://localhost:5050';
const url = 'https://virtual-flashcard-api.herokuapp.com';
const alertClose = document.getElementsByClassName('alert-close');

const unselect_prev_deck = () => {
    const activeDecks = document.getElementsByClassName('deck-item active');
    for (var i = 0; i < activeDecks.length; i++) {
        activeDecks[i].classList.remove('active');
    }
}

for (var i = 0; i < alertClose.length; i++) {
    alertClose[i].addEventListener('click', function() {
        this.closest('.alert').style.display = "none";
    })
}

