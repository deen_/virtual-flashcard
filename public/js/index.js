const cardFace = document.querySelector('.card-face');
const cardContent = document.querySelector('.card-content');
const card = document.querySelector('.card');
const shuffleBtns = document.getElementsByClassName('shuffle');
const shuffleAlert = document.querySelector('.alert-shuffle');
const next = document.querySelector('#next');
const deckContainer = document.querySelector('.deck-items');
const params = new URLSearchParams(window.location.search)

var decks = [];
var currentDeck = [];
var currentCardIndex = 0;
var currentFace = "FRONT";

const get_params_deck_id = () => {
    if (!params.has('d')) return 0;

    let id = decks.findIndex(deck => deck._id === params.get('d'));

    return id;
}

const retrieve_decks = () => {
    let newUrl = url + '/decks' + (logged_in() ? '/me?includePublic=true' : '');

    fetch(newUrl, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + userToken
        }
    }).then(response => {
        if (response.status !== 200) {
            console.log('Looks like there was a problem. Status Code: ' +
              response.status);
            return;
        }

        response.json().then((data) => {
            if (data.error) {
                console.log(data.error);
            } else {
                decks = data;

                if ( decks ) {
                    currentDeck = decks[ get_params_deck_id() ];
                    display_decks();
                    display_card();
                }
            }
        })
    })
}

const resizeToFit = () => {
    let fontSize = window.getComputedStyle(cardContent).fontSize;
    cardContent.style.fontSize = (parseFloat(fontSize) - 5) + 'px';
  
    if (cardContent.scrollWidth > cardContent.clientWidth ||
        cardContent.scrollHeight > cardContent.clientHeight
    )  resizeToFit();
}

const display_decks = () => {
    deckContainer.innerHTML = '';

    let publicDecks = decks.filter( el => el.public === true);
    let displayPublicHeader = false;

    decks.forEach( (deck, index) => {

        if (publicDecks.some(d => d.description === deck.description) && !displayPublicHeader) {
            deckContainer.innerHTML += `<p class="pt-10"><b>PUBLIC DECKS:</b></p>`;
            displayPublicHeader = true;
        }

        var li = document.createElement('li');
        li.className = 'deck-item';
        if (currentDeck._id === deck._id) li.classList.add('active');
        li.appendChild(document.createTextNode(
            deck.description + 
            (deck.public ? " by " + deck.creator : "" ) + 
            " ("+ deck.cards.length+" cards)"));

        if (deck.cards.length > 0) {
            li.addEventListener('click', function() {
                change_deck(index);
                unselect_prev_deck();
                this.classList.add('active');
            })    
        } 

        deckContainer.appendChild(li);
    })
}

const change_deck = index => {
    currentDeck = decks[index];
    currentCardIndex = 0;
    currentFace = "FRONT";
    display_card();
    changeDeckModal.style.display = "none";
}

const display_card = () => {
    cardFace.textContent = currentFace;
    cardContent.textContent = currentDeck.cards[currentCardIndex].front;
    cardContent .style.fontSize = '200px';
    resizeToFit();
}

const flip_card = () => {
    card.classList.toggle('flipped');

    if ( currentFace  == "FRONT") {
        currentFace = "BACK";
        cardContent.textContent = currentDeck.cards[currentCardIndex].back;
    } else {
        currentFace = "FRONT";
        cardContent.textContent = currentDeck.cards[currentCardIndex].front;
    }
    cardFace.textContent = currentFace;
    cardContent .style.fontSize = '200px';
    resizeToFit();
    
}

const shuffle_cards = () => {
    shuffleAlert.style.display = "block";
    let currentIndex = currentDeck.cards.length, randomIndex;
    
    while (currentIndex != 0) {
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex--;
    
        // And swap it with the current element.
        [currentDeck.cards[currentIndex], currentDeck.cards[randomIndex]] = [
        currentDeck.cards[randomIndex], currentDeck.cards[currentIndex]];
    }

    currentCardIndex = 0;

    setTimeout(function() {
        display_card();
        shuffleAlert.style.display = "none";
    }, 2000);
}

const next_card = () => {
    if (currentCardIndex < currentDeck.cards.length - 1) {
        currentCardIndex = currentCardIndex+1;
    } else {
        currentCardIndex = 0;
    }
    currentFace = "FRONT";

    // animation
    card.classList.remove("next");
    void card.offsetWidth; 
    card.classList.add("next");

    setTimeout(function() {
        display_card();
    }, 500);
}
card.addEventListener('click', flip_card);
next.addEventListener('click', next_card);

for (var i = 0; i < shuffleBtns.length; i++) {
    shuffleBtns[i].addEventListener('click', shuffle_cards);
}

retrieve_decks();