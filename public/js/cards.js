const cardsContainer = document.querySelector('.card-list');
const cardItems = document.getElementsByClassName('card-item');
const searchCard = document.querySelector('#search-card');
const updateCardModal = document.querySelector('.modal-update-card');
const pagination = document.querySelector('.pagination');
const playButtonContainer = document.querySelector('.play-button');

const createCardForm = document.querySelector('#create-card-form');
const updateCardForm = document.querySelector('#update-card-form');

var cardTotalCount = document.querySelector('.card-total-count');
var editFront = document.querySelector('#edit-front');
var editBack = document.querySelector('#edit-back');
var currentCard = "";

const playable = (status) => {
    if (status) playButtonContainer.style.display = "block";
    else playButtonContainer.style.display = "none";
}

const display_cards = () => {
    cardsContainer.innerHTML = '';
    cardTotalCount.textContent = 0;
    totalPages = 1;
    
    if ( currentDeck.cards && currentDeck.cards.length > 0 ) {

        cardTotalCount.textContent = currentDeck.cards.length;
        totalPages = Math.ceil(currentDeck.cards.length / perPage);

        currentDeck.cards.forEach( card => {
            
            cardsContainer.innerHTML += `<li class="card-item" id="card-${card._id}" style="display:none">
                <div class="card-item-content">
                    <div class="card-item-front">${card.front}</div>
                    <div class="card-item-back">${card.back}</div>
                    <div class="card-item-icons">
                        <a href="#"><i class="fa fa-edit modal-open update-card" data-target="modal-update-card" data-id="${card._id}"></i></a>
                        <a href="#"><i class="fa fa-trash delete-card" data-id="${card._id}"></i></a>
                    </div>    
                </div>
            </li>`;
        })
        document.querySelector(`#deck-${currentDeck._id} span`).textContent = currentDeck.cards.length;
        playable(true);
        refresh_page();
    } else {
        document.querySelector(`#deck-${currentDeck._id} span`).textContent = 0;
        playable(false);
    }

    display_pagination();
}

createCardForm.addEventListener('submit', (e) => {
    e.preventDefault();

    let front = document.getElementById('front-card');
    let back = document.getElementById('back-card');
    let body = {
        front: front.value,
        back: back.value,
    }
    
    fetch(url + "/decks/" + currentDeck._id + "/cards", {
        method: 'POST',
        body: JSON.stringify(body),
        headers: { 
            "Content-Type": "application/json",
            'Authorization': 'Bearer ' + userToken
        }
    }).then((res) => res.json())
    .then((data) => {
        front.value = "";
        back.value = "";
        currentDeck.cards.push(data);
        display_cards();
    })
    .catch((err) => console.log(err))
});

updateCardForm.addEventListener('submit', function (e) {
    e.preventDefault();

    let card = document.querySelector('#card-' + currentCard._id);
    let front = card.querySelector('.card-item-front');
    let back = card.querySelector('.card-item-back');
    let body = { 
        front: editFront.value,
        back: editBack.value,
    }
    
    fetch(url + "/decks/" + currentDeck._id + "/cards/" + currentCard._id, {
        method: 'PATCH',
        body: JSON.stringify(body),
        headers: { 
            "Content-Type": "application/json",
            'Authorization': 'Bearer ' + userToken
        }
    }).then((res) => res.json())
    .then((result) => {
        updateCardModal.style.display = "none";

        currentCard.front = editFront.value;
        currentCard.back = editBack.value;

        front.textContent = editFront.value;
        back.textContent = editBack.value;
    })
    .catch((err) => console.log(err))
})

document.addEventListener('click',function(e) {
    if ( e.target && e.target.classList.contains('update-card') ){
        let card_id = e.target.dataset.id;
        currentCard = currentDeck.cards.find(card => {
            return card._id === card_id
        });

        if ( currentCard ) {
            editFront.value = currentCard.front;
            editBack.value = currentCard.back;
        }
    }  
})

document.addEventListener('click',function(e) {
    if ( e.target && e.target.classList.contains('delete-card') ){
        let card_id = e.target.dataset.id;
        
        fetch( url + "/decks/" + currentDeck._id + "/cards/" + card_id, {
            method: 'DELETE',
            headers: { 
                "Content-Type": "application/json",
                'Authorization': 'Bearer ' + userToken
            }
        }).then((res) => res.json())
        .then((result) => {
            currentDeck.cards = currentDeck.cards.filter(function(value){ 
                return value._id != card_id;
            });
            display_cards();
        })
        .catch((err) => console.log(err))
    }  
})

searchCard.addEventListener('keyup', function(e) {
    let val = this.value.toUpperCase();

    if (val == "") {
        refresh_page();
        return;
    }

    Array.from(cardItems).filter((card) => {
        front = card.querySelector('.card-item-front').textContent.toUpperCase();
        back = card.querySelector('.card-item-back').textContent.toUpperCase();

        if (front.indexOf(val) > -1 || back.indexOf(val) > -1) {
            card.style.display = "block";
        } else {
            card.style.display = "none";
        }
    })
    
})