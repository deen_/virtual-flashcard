const logoutButton = document.querySelector('#logout');
const changePasswordForm = document.querySelector('#change-password-form');
const accountSettingsModal = document.querySelector('.modal-account-settings');
const deleteAccount = document.querySelector('#delete-account');
const alert = document.querySelector('.alert');
const greetUser = document.querySelector('.greet-user');

const logged_in = () => {
    return userToken != "" && userToken != undefined 
}

const getCookie = (name) => {
    const value = `; ${document.cookie}`;
    const parts = value.split(`; ${name}=`);
    if (parts.length === 2) return parts.pop().split(';').shift();
}

const deleteCookies = () => {
    var cookies = document.cookie.split(";");

    for (var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i];
        var eqPos = cookie.indexOf("=");
        var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
        document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
    }
}

let userToken = getCookie('userToken');
let user = getCookie('user');

if (logoutButton) {
    logoutButton.addEventListener('click', () => {
        deleteCookies();
        window.location = "/";
    })    
}

if (changePasswordForm) {
    changePasswordForm.addEventListener('submit', function(e) {
        e.preventDefault();
        let password = document.getElementById('password');
        let confirmPassword = document.getElementById('confirm-password');

        if ( password.value != confirmPassword.value ) {
            alert.style.display = "block";
            alert.classList.add('alert-danger');
            alert.querySelector('.message').textContent = "Password didn't matched!"
        } else {
            fetch(url + "/users/me", {
                method: 'PATCH',
                body: JSON.stringify({ password: password.value }),
                headers: { 
                    "Content-Type": "application/json",
                    'Authorization': 'Bearer ' + userToken
                }
            }).then((res) => res.json())
            .then((response) => {
                alert.style.display = "block";

                if (response.errors) {
                    alert.classList.add('alert-danger');
                    alert.querySelector('.message').textContent = response.message;
                } else {
                    alert.classList.add('alert-success');
                    alert.querySelector('.message').textContent = 'Successfully changed!';
                    password.value = "";
                    confirmPassword.value = "";
                }
            })
            .catch((err) => console.log(err))
        }
    })  
}

if (deleteAccount) { 
    deleteAccount.addEventListener('click', () => {
        fetch(url + "/users/me", {
            method: 'DELETE',
            body: JSON.stringify({ password }),
            headers: { 
                "Content-Type": "application/json",
                'Authorization': 'Bearer ' + userToken
            }
        }).then((res) => res.json())
        .then((data) => {
            deleteCookies();
            window.location = "/";
        })
        .catch((err) => console.log(err))
    })
}

if (user) {
    if (greetUser) greetUser.style.display = "block";
    document.querySelector('#logged-in-user').textContent = user;
}
else greetUser.style.display = "none";