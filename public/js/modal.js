const modals = document.getElementsByClassName("modal")
const modalCloseButtons = document.getElementsByClassName("modal-close")

const closeModal = function(e) {
    if ( e.target == e.currentTarget ) {
        this.closest('.modal').style.display = "none";
    }
}

for (var i = 0; i < modals.length; i++) {
    modals[i].addEventListener('click', closeModal, false);
}

for (var i = 0; i < modalCloseButtons.length; i++) {
    modalCloseButtons[i].addEventListener('click', closeModal, false);
}

document.addEventListener('click',function(e){
    if ( e.target && e.target.classList.contains('modal-open') ){
        let target = e.target.dataset.target;
        let modal = document.querySelector("." + target);
        modal.style.display = "block";
     }
});