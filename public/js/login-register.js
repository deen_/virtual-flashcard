const loginForm = document.querySelector('#login-form');
const registerForm = document.querySelector('#register-form');
const loginRegisterModalTitle = document.querySelector('#login-register-title');
const loginBtns = document.getElementsByClassName('login');
const registerBtns = document.getElementsByClassName('register');
const alertLogin = document.querySelector('#login-alert');

const setTokens = (response) => {
    userToken = response.token
    user = response.user.email
    document.cookie = "userToken=" + userToken;
    document.cookie = "user=" + user;
    greetUser.innerHTML = `Hi, ${user}! <a href="#" id="logout" class="hoverable"><i class="fa fa-sign-out"></i></a>`;   
}

loginForm.addEventListener('submit', function (e) {
    e.preventDefault();

    let fields = loginForm.elements;
    let body = {
        email: fields['email'].value,
        password: fields['password'].value,
    }
    
    fetch(url + "/users/login", {
        method: 'POST',
        body: JSON.stringify(body),
        headers: { "Content-Type": "application/json" }
    }).then((res) => res.json())
    .then((response) => {
        alertLogin.style.display = "block";
        if (response.error) {
            alertLogin.classList.add('alert-danger');
            alertLogin.querySelector('.message').textContent = response.error;
            return;
        } 

        alertLogin.classList.add('alert-success');
        alertLogin.querySelector('.message').textContent = "Successfully logged in!";

        setTokens(response);
        retrieve_decks();    

        setTimeout(function() {
            loginModal.style.display = "none";
        }, 1000);    
    })
    .catch((err) => console.log(err))
})

registerForm.addEventListener('submit', function (e) {
    e.preventDefault();
    let fields = registerForm.elements;
    let password = fields['password'].value;
    let confirmPassword = fields['confirm_password'].value;

    if ( password != confirmPassword ) {
        alertLogin.style.display = "block";
        alertLogin.classList.add('alert-danger');
        alertLogin.querySelector('.message').textContent = "Password didn't matched!";
        return;
    }

    let body = {
        name: fields['name'].value,
        email: fields['email'].value,
        password: password,
    }
    
    fetch(url + '/users', {
        method: 'POST',
        body: JSON.stringify(body),
        headers: { "Content-Type": "application/json" }
    }).then((res) => res.json())
    .then((response) => {
        alertLogin.style.display = "block";

        if (response.errors) {
            alertLogin.classList.add('alert-danger');
            alertLogin.querySelector('.message').textContent = response.error;
            return;
        }

        alertLogin.classList.add('alert-success');
        alertLogin.querySelector('.message').textContent = "Successfully registered!";
        
        setTokens(response);
        retrieve_decks();    

        setTimeout(function() {
            loginModal.style.display = "none";
        }, 1000); 
    })
    .catch((err) => console.log(err))
})

for (var i = 0; i < loginBtns.length; i++) {
    loginBtns[i].addEventListener('click', () => {
        loginForm.style.display = "block";
        registerForm.style.display = "none";
        loginRegisterModalTitle.textContent = "LOGIN";
    })
}

for (var i = 0; i < registerBtns.length; i++) {
    registerBtns[i].addEventListener('click', () => {
        loginForm.style.display = "none";
        registerForm.style.display = "block";
        loginRegisterModalTitle.textContent = "REGISTER";
    }) 
}
