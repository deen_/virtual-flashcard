const sidebar = document.querySelector('.sidebar');
const sidebarOpen = document.querySelector('.sidebar-open');
const sidebarClose = document.querySelector('.sidebar-close');
const deckContainer = document.querySelector('.deck-items');
const playButtons = document.getElementsByClassName('play-button');

const createDeckModal = document.querySelector('.modal-create-deck');
const updateDeckModal = document.querySelector('.modal-update-deck');
const deleteDeckModal = document.querySelector('.modal-delete-deck');

const createDeckForm = document.querySelector('#create-deck-form');
const updateDeckForm = document.querySelector('#update-deck-form');
const deleteDeck = document.querySelector('#delete-deck');

var editDescription = document.querySelector('#edit-description');
var editPublic = document.querySelector('#edit-public');
var deckTitle = document.querySelector('.panel-title');

var decks = [];
var currentDeck = [];

const retrieve_decks = () => {
    fetch(url + '/decks/me', {
        headers: { 
            "Content-Type": "application/json",
            'Authorization': 'Bearer ' + userToken
        }
    }).then((response) => {
        response.json().then((data) => {
            if (data.error) {
                console.log(data.error);
            } else {
                decks = data;

                if ( decks ) {
                    display_decks();
                }
            }
        })
    })
}

const display_decks = () => {
    deckContainer.innerHTML = '';

    decks.forEach( (deck, index) => {
        let li = document.createElement('li');
        let span = document.createElement('span');
        let cardsCount = 0;

        li.className = 'deck-item';
        li.setAttribute('id', 'deck-' + deck._id)
        if (index == 0) li.classList.add('active');
        if (deck.cards) cardsCount = deck.cards.length  
        
        li.appendChild(document.createTextNode(deck.description));
        span.appendChild(document.createTextNode(cardsCount));
        li.appendChild(span);
        li.addEventListener('click', function() {
            display_deck_details(index);
            unselect_prev_deck();
            this.classList.add('active');
        })  

        deckContainer.appendChild(li);
    })

    display_deck_details(0);
}

const display_deck_details = index => {
    currentDeck = decks[index];
    deckTitle.innerHTML =  `
        <a href="#" class="modal-open hoverable" data-target="modal-update-deck" data-id="${currentDeck._id}" id="update-deck">${currentDeck.description}</a>`;

    display_cards();
}

createDeckForm.addEventListener('submit', function (e) {
    e.preventDefault();

    let description = document.getElementById('description');
    let public = document.getElementById('public');
    let body = {
        description: description.value,
        public: public.checked
    }
    
    fetch(url + '/decks', {
        method: 'POST',
        body: JSON.stringify(body),
        headers: { 
            "Content-Type": "application/json",
            'Authorization': 'Bearer ' + userToken
        }
    }).then((res) => res.json())
    .then((data) => {
        decks.push(data);
        display_decks(); 
        createDeckModal.style.display = "none";
        description.value = "";
        public.checked = false;
    })
    .catch((err) => console.log(err))
})

updateDeckForm.addEventListener('submit', function (e) {
    e.preventDefault();

    let body = { 
        description: document.getElementById('edit-description').value,
        public: document.getElementById('edit-public').checked
     }
    
    fetch( url + "/decks/" + currentDeck._id, {
        method: 'PATCH',
        body: JSON.stringify(body),
        headers: { 
            "Content-Type": "application/json",
            'Authorization': 'Bearer ' + userToken
        }
    }).then((res) => res.json())
    .then((result) => {
        currentDeck = result;
        document.querySelector(`#update-deck`).textContent = currentDeck.description;
        document.querySelector(`#deck-${currentDeck._id}`).textContent = currentDeck.description;
        updateDeckModal.style.display = "none";
    })
    .catch((err) => console.log(err))
})

deleteDeck.addEventListener('click', () => {
    fetch( url + "/decks/" + currentDeck._id, {
        method: 'DELETE',
        headers: { 
            "Content-Type": "application/json",
            'Authorization': 'Bearer ' + userToken
        }
    }).then((res) => res.json())
    .then((result) => {
        decks = decks.filter(function(value, index, arr){ 
            return value._id != currentDeck._id;
        });
        display_decks();
        deleteDeckModal.style.display = "none";
    })
    .catch((err) => console.log(err))
})

document.addEventListener('click',function(e){
    if ( e.target && e.target.id == "update-deck" ){
        editDescription.value = currentDeck.description;
        editPublic.checked = currentDeck.public;
    }  
})



sidebarOpen.addEventListener('click', () => {
    sidebar.classList.toggle('open');
})

sidebarClose.addEventListener('click', () => {
    sidebar.classList.toggle('open');
})

for (var i = 0; i < playButtons.length; i++) {
    playButtons[i].addEventListener('click', () => {
        window.location = "/?d=" + currentDeck._id;
    })
}

retrieve_decks();